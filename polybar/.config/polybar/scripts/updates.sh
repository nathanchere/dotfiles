#!/bin/bash

pac=$(checkupdates 2> /dev/null | wc -l)
aur=$(yay -Qum 2> /dev/null | wc -l)

total=$(("$pac" + "$aur"))
result='Up to date'
if [ "$total" -gt 0 ]; then
    result=''
    if [[ "$pac" != "0" ]]; then
        result="PAC[$pac]"
    fi
    
    if [[ "$aur" != "0" ]]; then
        result="$result AUR[$aur]"
    fi
fi

echo $result
