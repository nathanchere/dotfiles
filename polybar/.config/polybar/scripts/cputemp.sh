#!/bin/bash

# cat /sys/class/hwmon/hwmon0/temp1_input
# acpi -t


# tempGPU=$(sensors | grep temp1 | head -n1 | sed -r 's/.*:\s+[\+-]?(.*C)\s+.*/\1/')
# temp=$(sensors | grep -i PECI | head -n1 | sed -r 's/.*:\s+[\+-]?(.*C)\s+.*/\1/')

temp=$(sensors|awk 'BEGIN{i=0;t=0;b=0}/id [0-9]/{b=$4};/Core/{++i;t+=$3}END{if(i>0){printf("%0.1f\n",t/i)}else{sub(/[^0-9.]/,"",b);print b}}')

printf " CPU: $temp°C"
