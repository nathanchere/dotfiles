#!/bin/bash

t=0

toggle() {
    t=$(((t + 1) % 2))
}

togb() {
  echo "scale=1; $1 / 1024^2" | bc
}

trap "toggle" USR1

while true; do

  RAM="$(vmstat -s | grep -P 'total memory|used memory' | awk '{ print $1 }')"
  RAM_TOTAL="$(echo $RAM | awk -F ' ' '{ print $1 }' )"
  RAM_CURRENT="$(echo $RAM | awk -F ' ' '{ print $2 }' )"

  RAM_USAGE="$(echo "scale = 2; $RAM_CURRENT/$RAM_TOTAL*100" | bc)"
  RAM_USAGE="$(printf "%2.0f" "$RAM_USAGE")"

  if [ $RAM_USAGE -gt 90 ]; then
    color="#FF0000"
  elif [ $RAM_USAGE -gt 80 ]; then
    color="#FF7800"
  fi

  if [ $t -eq 0 ]; then
    echo " $RAM_USAGE%"
  else
    echo " $(togb $RAM_CURRENT)/$(togb $RAM_TOTAL)Gb ($RAM_USAGE%)"
  fi
  sleep 1 &
  wait
done


