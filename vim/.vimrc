syntax enable " enable syntax highlighting
set tabstop=4
set softtabstop=4
set expandtab " turn tabs into spaces
set number " show line numbers
set showcmd " echo last command in bottom right corner
set cursorline " highlight current line
filetype indent on " TODO: indent files
set wildmenu
set lazyredraw
set showmatch
set incsearch " search as you type / incremental search
set hlsearch " highlight search