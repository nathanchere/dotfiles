"https://gist.github.com/prkstaff/a8d9ab54318e462f0321444bba472504"
" https://dougblack.io/words/a-good-vimrc.html
" http://www.smoothterminal.com/articles/neovim-for-elixir
set tabstop=4
set softtabstop=4
set expandtab " turn tabs into spaces
set number " show line numbers
set showcmd " echo last command in bottom right corner
set cursorline " highlight current line
filetype indent on " TODO: indent files
set wildmenu
set lazyredraw
set showmatch
set incsearch " search as you type / incremental search
set hlsearch " highlight search

let g:vim_bootstrap_langs ='javascript,python,elixir,csharp,elm,fish,erlang,fsharp,dockerfile,haskell,html5,jsx,lua,markdown,kotlin,rust,scss,typescript,vue'
let g:vim_bootstrap_editor = 'nvim'				" nvim or vim

let vimplug_exists=expand('~/.config/nvim/autoload/plug.vim')
if !filereadable(vimplug_exists)
  echo "Installing Vim-Plug..."
  echo ""
  silent !\curl -fLo ~/.config/nvim/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
  let g:not_finish_vimplug = "yes"

  autocmd VimEnter * PlugInstall
endif

call plug#begin(expand('~/.config/nvim/plugged'))
Plug 'scrooloose/nerdtree'
Plug 'jistr/vim-nerdtree-tabs'
"Plug 'tpope/vim-commentary'
"Plug 'tpope/vim-fugitive'
Plug 'ctrlpvim/ctrlp.vim'
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'airblade/vim-gitgutter'
Plug 'bronson/vim-trailing-whitespace'
Plug 'scrooloose/syntastic'
Plug 'Yggdroot/indentLine'
Plug 'OmniSharp/omnisharp-vim'
Plug 'davidhalter/jedi-vim'
Plug 'ryanoasis/vim-devicons'
Plug 'avelino/vim-bootstrap-updater'

" themes
Plug 'drewtempelmeyer/palenight.vim'
Plug 'arcticicestudio/nord-vim'
call plug#end()



if (has("termguicolors"))
 set termguicolors
endif
syntax enable
colorscheme Oceanic
" colorscheme nord

"let g:lightline.colorscheme = 'palenight'

filetype plugin indent on
set encoding=utf-8
set fileencoding=utf-8
set fileencodings=utf-8
set bomb
set binary
set backspace=indent,eol,start

set tabstop=4
set softtabstop=0
set shiftwidth=4
set expandtab

let mapleader=','