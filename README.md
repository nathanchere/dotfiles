# Totally uninteresting dotfies

Just my Linux workstation config.

##

Broken off from [.ncx](https://github.com/nathanchere/.ncx) to distinguish config versioning from the .ncx tool itself.

I don't expect anyone else to really be interested in these but if you do have any questions about WTF I'm doing, feel free to open an issue on here.

## Configuration Summary

### X

* Display manager: TBC
* Window manager: XMonad
* Bars/tray/etc: polybar, tint2
* Notifications: dunst
* Launcher: rofi
* Shell: fish (w/ ohmyfish), xonsh
* Terminal: terminator (main), alacritty, urxvt (scratchpad)

### Wayland

* Window manager: hyprland
* Bars/tray/etc: waybar
* Notifications: mako
* Launcher: rofi
* Shell: fish, xonsh
* Terminal: foot

## hyprland keybindings cheat sheet

* [Meta]+[Enter] : launch new terminal
* [Meta]+[E] : launch file manager
* [Meta]+[R] : run command
* [Meta]+[Shift]+[R] : run application by name
* [Meta]+[W] : close focused window
* [Meta]+[V] : show clipboard manager
* [Meta]+[A] : toggle scratchpad
* [Meta]+[PrintScreen] : take screenshot
* [Meta]+[Meta]+[PrintScreen] : colour picker

* [Meta]+[left click drag] : move window 
* [Meta]+[right click drag] : resize window 

* [Meta]+[CursorKey] : move window focus
* [Meta]+[Shift}+[CursorKey] : move window
* [Meta]+[Alt}+[CursorKey] : resize focused window
* [Meta]+[T] : toggle window as tiled/floating 
* [Meta]+[Space] : toggle fullscreen
* [Meta]+[Tab] : cycle window focus (master layout only)
* [Meta]+[Shift]+[Tab] : cycle between 'master' layouts
* [Meta]+[Shift]+[Space] : swap with master tile

* [Meta]+[L] : to "dwindle" layout
* [Meta]+[Shift]+[L] : to "master" layout

## xmonad keybindings cheat sheet

* Win+Shift+Enter : open new terminal (terminator)
* Win+[1..9] : switch to workspaces n
* Win+Shift+[1..9] : send focused window to workspace n
* Win+Space :  toggle window layout for current workspace
* Win+R : run dialog (rofi)
* Win+Shift+R : run application by name (rofi)
* Win+\` : drop-down terminal (urxvt)
---
* Win+`Left Click` : drag to un-tile and freely position selected window
* Win+`Right Click` : drag to resize selected window
* Win+T : re-tile focused floating window
---
* VolumeUp : increase volume by 5%
* VolumeDown : decrease volume by 5%
* Ctrl+VolumeUp : set volume to maximum
* Ctrl+VolumeDown : set volume to minimum
* BrightnessUp : increase backlight brightness by 5%
* BrightnessDown : decrease backlight brightness by 5%
* Ctrl+BrightnessUp : set backlight brightness to maximum
* Ctrl+BrightnessDown : set backlight brightness to minimum
---
* Win+Q : recompile / restart Xmonad
-------
* [Meta]+[Alt]+[L] : lock the session 
* Win+Shift+C : kill focused window
* ? : monitor focusing
* ? : dmenu / gmrun ?


## rofi

* Win+R : run by filename dialog (rofi)
* Win+Shift+R: run by application name dialog (rofi)

Rofi includes:

* calc module (requires install separately - `paru rofi calc`)

### Typing weird Swedish letters without an ISO keyboard

`<Right-Alt>` is the multi-input key. Hold and press for:

* `.` `a` - `å`
* `'` `a` - `ä`
* `'` `a` - `ä`

### Documentation for various things

* rofi [configuration](https://github.com/davatorium/rofi/blob/next/doc/rofi-theme.5.markdown)
* tint2 
  - [basic configuration](https://gitlab.com/o9000/tint2/blob/master/doc/tint2.md)
  - [third-party applets](https://gitlab.com/o9000/tint2/-/wikis/ThirdPartyApplets)

### Thanks / inspiration

References which helped me a lot along the way:

* bash
    * https://dev.to/thiht/shell-scripts-matter
    * http://robertmuth.blogspot.se/2012/08/better-bash-scripting-in-15-minutes.html
    * https://jvns.ca/blog/2017/03/26/bash-quirks/
    * http://bash3boilerplate.sh/

* i3
    * https://gitlab.com/Hashfastr/i3-config-files

* General tips and tricks
  * https://gitlab.com/tomah/dotrubin (using rofi as a 'Power' menu)

* xmonad
    * https://github.com/Fulmene/dotfiles
    * https://github.com/jaagr
    

* specifics
  * rofi powermenu - https://github.com/tostiheld

* waybar, hyprland etc
  * https://github.com/7KIR7/dots
  * https://gitlab.com/fazzi/dotfiles
  * https://github.com/ThatTakashi/DotFileswaybar
  * https://github.com/prasanthrangan/hyprdots
  * https://codeberg.org/JustineSmithies/hyprland-dotfiles
  * https://github.com/lokesh-krishna/dotfiles
 
* polybar
  * https://gitlab.com/tomah/dotrubin

  * https://wiki.haskell.org/Xmonad/Config_archive/John_Goerzen's_Configuration
* https://github.com/davidbrewer/xmonad-ubuntu-conf
* https://github.com/ktoso/xmonad-conf
* rawsh
* joedicastro
* xero
* sn0rlax
* leoxiong
* ethanschoonover
* [adi1090x](https://github.com/adi1090x) polybar
* https://github.com/jp1995/dotfiles/
* https://github.com/zoresvit/dotfiles
* https://github.com/foo-jin/dotfiles
* https://github.com/Fulmene/dotfiles/blob/master/xmonad/.xmonad/lib/XMonad/Fulmene/KeyBindings.hs
* https://github.com/pschmitt/xmonad-config/blob/master/xmonad.hs
* https://gitlab.com/dwt1/dotfiles
* https://adrian-philipp.com/post/cmd-duration-fish-shell 

Apologies to anyone else who I've stolen code or techniques from but forgotten to mention

