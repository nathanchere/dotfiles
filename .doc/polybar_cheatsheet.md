## Styling

https://github.com/polybar/polybar/wiki/Formatting

Setting colour inline

```
; Foregound
ramp-capacity-0 = %{F#0F0}%{F-}

; Background
ramp-capacity-0 = %{B#F00}%{B-}

; Both
ramp-capacity-0 = %{F#FFF}%{B#FF0}%{B-}%{F-}
```
