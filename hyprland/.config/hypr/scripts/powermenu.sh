#!/usr/bin/env sh

waylogout \
	--hide-cancel \
	--screenshots \
	--font="Iosevka Nerd Font" \
	--fa-font="Font Awesome 6 Free" \
	--effect-blur=7x5 \
	--indicator-thickness=20 \
	--ring-color=888888aa \
	--inside-color=88888866 \
	--text-color=eaeaeaaa \
	--line-color=00000000 \
	--ring-selection-color=33cc33aa \
	--inside-selection-color=33cc3366 \
	--text-selection-color=eaeaeaaa \
	--line-selection-color=00000000 \
	--lock-command="swaylock" \
	--logout-command="hyprctl dispatch exit" \
	--reboot-command="reboot" \
	--poweroff-command="shutdown" \
	--selection-label


		# --suspend-command="echo suspend" \
	# --hibernate-command="echo hibernate" \
	# --switch-user-command="hyprctl dispatch exit" \
