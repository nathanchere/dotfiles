#!/usr/bin/env sh

gaps=$(hyprctl getoption general:gaps_out | grep int | awk '{print $2}')

if [ $gaps -gt 0 ]
then
    hyprctl keyword general:gaps_in 0
    hyprctl keyword general:gaps_out 0
    hyprctl keyword general:border_size 2
    hyprctl keyword decoration:rounding 0
    # hyprctl keyword decoration:drop_shadow false
else
    hyprctl keyword general:gaps_in 5
    hyprctl keyword general:gaps_out 8
    hyprctl keyword general:border_size 3
    hyprctl keyword decoration:rounding 8
    # hyprctl keyword decoration:drop_shadow true
fi
