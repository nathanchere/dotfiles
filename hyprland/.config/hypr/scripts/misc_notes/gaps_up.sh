#!/usr/bin/env sh

gaps=$(hyprctl getoption general:gaps_in | grep int | awk '{print $2}')
gaps=$(expr $gaps + 2)
hyprctl keyword general:gaps_in $gaps
hyprctl keyword general:gaps_out $gaps
