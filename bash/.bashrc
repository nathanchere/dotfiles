#!/usr/bin/env bash

[[ $- != *i* ]] && return


if [ -f /etc/bashrc ]; then
    . /etc/bashrc
fi

export DOTNET_CLI_TELEMETRY_OPTOUT=1
export MSBuildSDKsPath=/usr/share/dotnet/sdk/$(dotnet --version)/Sdks/
export EDITOR=nvim
export QT_STYLE_OVERRIDE=gtk
export QT_SELECT=qt5

if [[ $LANG = '' ]]; then
    export LANG=en_AU.UTF-8
fi

alias ls='lsd -l'

# >>> conda initialize >>>
# !! Contents within this block are managed by 'conda init' !!
__conda_setup="$('/home/norfen/.local/share/miniconda3/bin/conda' 'shell.bash' 'hook' 2> /dev/null)"
if [ $? -eq 0 ]; then
    eval "$__conda_setup"
else
    if [ -f "/home/norfen/.local/share/miniconda3/etc/profile.d/conda.sh" ]; then
        . "/home/norfen/.local/share/miniconda3/etc/profile.d/conda.sh"
    else
        export PATH="/home/norfen/.local/share/miniconda3/bin:$PATH"
    fi
fi
unset __conda_setup
# <<< conda initialize <<<

. /opt/asdf-vm/asdf.sh

