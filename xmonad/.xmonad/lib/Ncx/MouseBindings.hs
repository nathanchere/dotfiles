module Ncx.MouseBindings(myMouseBindings) where

import           XMonad
import qualified XMonad.Actions.FlexibleResize as Flex
import           XMonad.Actions.WindowGo
import qualified Data.Map                      as M
import qualified XMonad.StackSet               as W
import           XMonad.Hooks.SetWMName

myMouseBindings (XConfig { XMonad.modMask = modm }) =
    M.fromList
      $ [ ( (modm, button1)
          , (\w -> focus w >> mouseMoveWindow w >> windows W.shiftMaster)
          )
        , ((modm, button2), (\w -> focus w >> windows W.shiftMaster))
        , ( (modm, button3)
          , (\w -> focus w >> Flex.mouseResizeWindow w >> windows W.shiftMaster)
          )
        ]