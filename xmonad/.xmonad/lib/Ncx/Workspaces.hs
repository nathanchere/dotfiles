module Ncx.Workspaces(myWorkspaces) where

-- workspace1 = "1:\xf121" -- Code
-- workspace2 = "2:\xf02d" -- Stuff


-- TODO: workspaces 9..16 for 'minimised' windows
-- nScreens
--   = countScreens
myWorkspaces =
    --  withScreens nScreens
   map show [1..6]
   -- map show [1..3] ++ ["d", "e"]