module Ncx.KeyBindings
  ( myKeys
  ) where

import           Ncx.Applications
import           Ncx.NamedScratchpad
import           XMonad
import           XMonad.Actions.WindowGo
import           XMonad.Hooks.ManageDocks
import           XMonad.Layout.MultiToggle
import           XMonad.Layout.MultiToggle.Instances
import           XMonad.Layout.Reflect
import           XMonad.Prompt.RunOrRaise            (runOrRaisePrompt)
import           XMonad.Util.NamedScratchpad

myKeys = myMediaKeys <+> myOtherKeys <+> myLayoutKeys

myLayoutKeys =
  [ (("M4-["), sendMessage $ Toggle REFLECTX)
  , (("M4-]"), sendMessage $ Toggle REFLECTY)
  , (("M4-\\"), sendMessage $ ToggleStruts)
  , (("M4-="), sendMessage $ Toggle MIRROR)
  -- , ("M4-<Space>", sendMessage $ Toggle FULL)
  -- , ((myModMask, xK_s), sendMessage MirrorShrink)
  -- , ((myModMask, xK_x), sendMessage MirrorExpand)
    -- , ("M-C-s", sendMessage $ pullGroup WN.L) --sublayout --artistgamer --merge left to tab stack
    -- , ("M-C-f", sendMessage $ pullGroup WN.R) --sublayout --artistgamer --merge right to tab stack
    -- , ("M-C-e", sendMessage $ pullGroup WN.U) --sublayout --artistgamer --merge up to tab stack
    -- -- , ("M-C-d", sendMessage $ pullGroup WN.D) --sublayout --artistgamer --merge down to tab stack
    -- , ("M-S-<F11>", sendMessage $ Toggle MIRROR) --mirror toggle.  yus yus.
    -- , ("M-g", sendMessage $ Toggle FULL) --artistgamer --fullscreen toggle. yus.
    -- , ("M-S-g", sendMessage $ Toggle MIRROR) --artistgamer --mirror toggle.  yus yus.
  ]

myOtherKeys =
  [ (("M4-`"), namedScratchpadAction myScratchpad "terminal")
    -- OS Misc
  , (("M4-r"), spawn myLauncherCommand)
  , (("M4-S-l"), spawn myScreenLock)
  , (("M4-S-r"), spawn myLauncherApplication)
  , (("M4-e"), spawn myFileManager)
  --   , (("M4-S-Tab"), spawn myLauncherWindow) -- TODO doesn't work - where is it being overriden?
  , (("M4-M1-l"), runOrRaise "lock" (className =? "i3lock"))
  , (("<Print>"), spawn myScreenshot)
  , (("M4-N"), spawn myNotificationsToggle)
    --,(("M4-S-Q", io (exitWith ExitSuccess))
  , (("M4-S-Q", spawn "notify-send quitting ok"))
    -- , ("M-<F6>", spawn "xkill") -- no, srsly, close it! (click window to kill)
  ]

myMediaKeys =
  [ (("<XF86MonBrightnessUp>"), spawn "ncx backlight +5%")
  , (("<XF86MonBrightnessDown>"), spawn "ncx backlight -5%")
  , (("S-<XF86MonBrightnessUp>"), spawn "ncx backlight max")
  , (("S-<XF86MonBrightnessDown>"), spawn "ncx backlight min")
  , (("<XF86AudioLowerVolume>"), spawn "amixer set Master 5%-")
  , (("<XF86AudioRaiseVolume>"), spawn "amixer set Master 5%+")
  , (("C-<XF86AudioLowerVolume>"), spawn "amixer set Master 1%")
  , (("C-<XF86AudioRaiseVolume>"), spawn "amixer set Master 100%")
  , (("<XF86AudioPlay>"), spawn "notify-send play")
  , (("<XF86AudioPause>"), spawn "notify-send pause") -- not used on XPS13?
  , (("<XF86AudioPrev>"), spawn "notify-send back")
  , (("<XF86AudioNext>"), spawn "notify-send forward")
  , (("<XF86Search>"), spawn "notify-send search")
  ]
