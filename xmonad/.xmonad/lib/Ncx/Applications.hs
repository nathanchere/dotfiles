module Ncx.Applications
  ( myTerminal
  , myScratchpadTerminal
  , myLauncherCommand
  , myLauncherApplication
  , myLauncherWindow
  , myScreenLock
  , myFileManager
  , myScreenshot
  , myNotificationsToggle
  , pipe
  , andThen
  , orElse
  , input
  , stdoutRedir
  ) where

myTerminal = "terminator"

myScratchpadTerminal = "terminator -p scratchpad -c scratchpadTerminal"

myFileManager = "caja"

myLauncherCommand = "rofi -show run"

myLauncherApplication = "rofi -show drun"

myLauncherWindow = "rofi -show window"

myScreenLock = "i3lock-fancy"

myScreenshot = "snip"

myToggleCompositor = "pkill compton" `orElse` "compton"

myNotificationsToggle = "kill -s USR1 $(pidof deadd-notification-center)"

myNotifySend notif = "notify-send" `input` notif

-- Utility functions
pipe cmd1 cmd2 = cmd1 ++ " | " ++ cmd2

andThen cmd1 cmd2 = cmd1 ++ " && " ++ cmd2

orElse cmd1 cmd2 = cmd1 ++ " || " ++ cmd2

input cmd inp = cmd ++ " \"" ++ inp ++ "\""

stdoutRedir cmd out = cmd ++ " > " ++ out
