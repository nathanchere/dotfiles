module Ncx.NamedScratchpad
  ( myScratchpad
  ) where

import           Ncx.Applications
import           XMonad
import qualified XMonad.StackSet             as S
import           XMonad.Util.NamedScratchpad

myScratchpad = [NS "terminal" spawnTerm (role =? "scratchpad") manageTerm]
  where
    -- -pscratchpad loads the "scratchpad" terminator profile (to distinguish styling from normal terminal windows)
    -- -r scratchpad sets the window class so xmonad can find and reuse the same scratchpad terminal instance
    spawnTerm = "terminator -pscratchpad -r scratchpad"
    role = stringProperty "WM_WINDOW_ROLE"
    -- findTerm = stringProperty "WM_WINDOW_ROLE" =? "scratchpad"
      -- resource=?"scratchpadTerminal"
    -- findTerm = (title =? "tmux")
    -- findTerm = resource =? "scratchpad"
    manageTerm = customFloating $ S.RationalRect left top width height
      where
        height = 0.4
        width = 0.6
        top = 0
        left = (1 - width) * 0.5
