module Ncx.Layouts
  ( myLayoutHook
  -- , mySpacingWidth
  , myNormalBorderColor
  , myFocusedBorderColor
  ) where

import           XMonad
import           XMonad.Hooks.EwmhDesktops
import           XMonad.Hooks.ManageDocks
import           XMonad.Layout.Circle
import           XMonad.Layout.DecorationMadness
import           XMonad.Layout.DragPane
import           XMonad.Layout.Gaps
import           XMonad.Layout.IndependentScreens
import           XMonad.Layout.MouseResizableTile
import           XMonad.Layout.MultiToggle
import           XMonad.Layout.MultiToggle.Instances
import           XMonad.Layout.NoBorders
import           XMonad.Layout.Reflect
import           XMonad.Layout.Spacing
import           XMonad.Layout.StackTile
import           XMonad.Layout.Tabbed
import qualified XMonad.Util.Themes                  as Themes

myFocusedBorderColor = "#BBFF00"

myNormalBorderColor = "#778877"

myLayoutHook =
  gaps [(U, 3), (R, 2), (D, 40)] $
  spacingRaw True noBorder True myBorder True tiled ||| tiled2 ||| full
  where
    mySpacingWidth = 2
    myBorderWidth = 10
    noBorder = Border 0 0 0 0
    myBorder = Border myBorderWidth 20 myBorderWidth 10
    tiled
      -- smartBorders $
     =
      mouseResizableTile
        {draggerType = FixedDragger {gapWidth = 0, draggerWidth = 4}}
    tiled2
      -- smartBorders $
     =
      mouseResizableTile
        { isMirrored = True
        , draggerType = FixedDragger {gapWidth = 0, draggerWidth = 4}
        }
    full = noBorders Full
    -- tall = Tall 1 0.03 0.5
    -- tilingDeco l =
    --   windowSwitcherDecorationWithButtons
    --     shrinkText
    --     bluetileTheme
    --     (draggingVisualizer l)
    -- floatingDeco l = buttonDeco shrinkText bluetileTheme l
      -- spacingWithEdge mySpacingWidth $
  -- smartBorders $
  -- avoidStruts $
    -- mkToggle (single REFLECTX) $
    -- mkToggle (single REFLECTY) $
  -- tall |||
-- myLayoutHook =
--   smartBorders $ avoidStruts $ spacingRaw mySpacingWidth $ mouseResizableTile
-- --  = avoidStruts $ (mouseResizableTile ||| stackTile ||| Full)
--   where
--     stackTile = StackTile 1 (3 / 100) (1 / 2)
-- -- import           XMonad.Layout.DecorationMadness
-- -- import           XMonad.Layout.DragPane
-- -- import           XMonad.Layout.IndependentScreens
-- -- import           XMonad.Layout.MouseResizableTile
-- -- import           XMonad.Layout.MultiToggle
-- -- import           XMonad.Layout.NoBorders          (smartBorders)
-- -- import           XMonad.Layout.Reflect
-- -- import           XMonad.Layout.Spacing
-- -- import           XMonad.Layout.Tabbed
-- -- ayoutHook = smartBorders
-- --                  $ WN.windowNavigation
-- --                  $ subTabbed
-- --                  $ boringWindows
-- --                  $
-- --                  mkToggle (MIRROR ?? NOBORDERS ?? FULL ?? EOT) (Grid (55/34) ||| OneBig (5/8) (3/5) ||| OneBig (3/4) (3/4))
-- -- Define group of default layouts used on most screens, in the
-- -- order they will appear.
-- -- "smartBorders" modifier makes it so the borders on windows only
-- -- appear if there is more than one visible window.
-- -- "avoidStruts" modifier makes it so that the layout provides
-- -- space for the status bar at the top of the screen.
-- defaultLayouts = smartBorders(avoidStruts(
--   -- ResizableTall layout has a large master window on the left,
--   -- and remaining windows tile on the right. By default each area
--   -- takes up half the screen, but you can resize using "super-h" and
--   -- "super-l".
--   ResizableTall 1 (3/100) (1/2) []
--   -- Mirrored variation of ResizableTall. In this layout, the large
--   -- master window is at the top, and remaining windows tile at the
--   -- bottom of the screen. Can be resized as described above.
--   ||| Mirror (ResizableTall 1 (3/100) (1/2) [])
--   -- Full layout makes every window full screen. When you toggle the
--   -- active window, it will bring the active window to the front.
--   ||| noBorders Full
--   -- Grid layout tries to equally distribute windows in the available
--   -- space, increasing the number of columns and rows as necessary.
--   -- Master window is at top left.
--   ||| Grid
--   -- ThreeColMid layout puts the large master window in the center
--   -- of the screen. As configured below, by default it takes of 3/4 of
--   -- the available space. Remaining windows tile to both the left and
--   -- right of the master window. You can resize using "super-h" and
--   -- "super-l".
--   ||| ThreeColMid 1 (3/100) (3/4)))
-- -- Here we define some layouts which will be assigned to specific
-- -- workspaces based on the functionality of that workspace.
-- -- The chat layout uses the "IM" layout. We have a roster which takes
-- -- up 1/8 of the screen vertically, and the remaining space contains
-- -- chat windows which are tiled using the grid layout. The roster is
-- -- identified using the myIMRosterTitle variable.
-- chatLayout = avoidStruts(withIM (1%7) (Title myIMRosterTitle) Grid)
-- -- The GIMP layout uses the ThreeColMid layout. The traditional GIMP
-- -- floating panels approach is a bit of a challenge to handle with xmonad;
-- -- I find the best solution is to make the image you are working on the
-- -- master area, and then use this ThreeColMid layout to make the panels
-- -- tile to the left and right of the image. If you use GIMP 2.8, you
-- -- can use single-window mode and avoid this issue.
-- gimpLayout = smartBorders(avoidStruts(ThreeColMid 1 (3/100) (3/4)))
-- -- Here we combine our default layouts with our specific, workspace-locked
-- -- layouts.
-- myLayouts =
--     onWorkspace "7:Chat" chatLayout
--   $ onWorkspace "9:Amarok" gimpLayout
--   $ defaultLayouts
