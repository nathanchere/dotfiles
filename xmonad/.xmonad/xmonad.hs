
--TODO: investigate
-- ezconfig
-- cycleRecentWS
-- hintedTile
-- TODO: detect fullscreening and refresh region metrics
-- TODO presentation mode (toggle borders, bars)
-- https://www.reddit.com/r/xmonad/comments/c3ss6l/split_xmonadhs_into_multiple_files/
-- https://www.reddit.com/r/xmonad/comments/c95fi0/workaround_for_full_screen_issues/
-- https://www.reddit.com/r/xmonad/comments/cm8pt4/addtabs_doesnt_show_tabs_with_tall/
-- https://www.reddit.com/r/xmonad/comments/d6sf32/what_resources_can_i_use_to_learn_how_to_create_a/
-- https://github.com/ktoso/xmonad-conf
-- https://wiki.haskell.org/Xmonad/Frequently_asked_questions
-- https://hackage.haskell.org/package/xmonad-contrib-0.16/docs/XMonad-Layout-IndependentScreens.html
-- https://github.com/pschmitt/xmonad-config/blob/master/xmonad.hs
-- https://www.reddit.com/r/xmonad/comments/d96upk/resize_tiling_layout_with_mouse_dragging_similar/
-- clean xmonad quit script -= https://github.com/thblt/DotFiles/blob/master/.xmonad/quit-xmonad.sh
-- good xmonad multimon setup https://github.com/IvanMalison/dotfiles/blob/d49eb65e7eb06cff90e171c0f5c44d4dae3a5510/dotfiles/xmonad/xmonad.hs#L671
-- xmonad conf https://static.charlieharvey.org.uk/src/xmonad.hs.txt
-- vodik dots
-- import           XMonad.Actions.CycleRecentWS
-- import qualified XMonad.Actions.FlexibleResize as Flex
-- import           XMonad.Actions.WindowGo
import           XMonad
import           XMonad.Config.Desktop
import           XMonad.Hooks.EwmhDesktops
import           XMonad.Hooks.ManageDocks
import           XMonad.ManageHook
import           XMonad.Util.EZConfig
import           XMonad.Util.NamedScratchpad

import qualified Data.Map                     as M
import qualified XMonad.StackSet              as S

import           Graphics.X11.ExtraTypes.XF86
import           XMonad.Hooks.SetWMName

import           Ncx.Applications
import           Ncx.KeyBindings
import           Ncx.Layouts
import           Ncx.MouseBindings
import           Ncx.NamedScratchpad
import           Ncx.Workspaces

myModMask = mod4Mask

myManageHook =
  floatManageHook <+> manageDocks <+> namedScratchpadManageHook myScratchpad

myBorderWidth = 0

-- to get class names:  xprop | grep WM_CLASS
floatManageHook :: ManageHook
floatManageHook =
  composeAll
    [className =? "temps" --> doFloat, className =? "terminator" --> doFloat]
  where
    unfloat = ask >>= doF . S.sink

myConfig =
  desktopConfig
    { terminal = myTerminal
    , modMask = myModMask
 -- TODO: handleEventHook
 -- TODO: LogHOok
    , borderWidth = myBorderWidth
    , focusedBorderColor = myFocusedBorderColor
    , normalBorderColor = myNormalBorderColor
    , mouseBindings = myMouseBindings
    , layoutHook = myLayoutHook
    , workspaces = myWorkspaces
    , manageHook = myManageHook <+> (className =? "temps" --> doIgnore)
    , startupHook = setWMName "LG3D"
    } `additionalKeysP`
  myKeys

main = do
  xmonad $ ewmh myConfig
  -- main = do
    -- xmproc <- spawnPipe "xmobar"
    -- xmonad $ defaultConfig
    --     { manageHook = manageDocks <+> manageHook defaultConfig
    --     , layoutHook = avoidStruts  $  layoutHook defaultConfig
    --     , logHook = dynamicLogWithPP xmobarPP
    --                     { ppOutput = hPutStrLn xmproc
    --                     , ppTitle = xmobarColor "green" "" . shorten 50
    --                     }
    --     , modMask = mod4Mask     -- Rebind Mod to the Windows key
    --     } `additionalKeys`
    --     [ ((mod4Mask .|. shiftMask, xK_z), spawn "xscreensaver-command -lock; xset dpms force off")
    --     , ((controlMask, xK_Print), spawn "sleep 0.2; scrot -s")
    --     , ((0, xK_Print), spawn "scrot")
    --     ]
--     myManagementHooks :: [ManageHook]
-- myManagementHooks = [
--   resource =? "synapse" --> doIgnore
--   , resource =? "stalonetray" --> doIgnore
--   , className =? "rdesktop" --> doFloat
--   , appName =? "sun-awt-X11-XWindowPeer" --> doFloat
--   , className =? "Yakuake" --> doFloat
--   , className =? "Exe" --> doFloat
--   , className =? "vlc" --> doF (W.shift "Media")
--   , className =? "Thunderbird" --> doF (W.shift "3:Mail")
--   , className =? "Plasma-desktop" --> doFloat
--   , className =? "pantheon-notify" --> doFloat
--   , (className =? "Komodo IDE") --> doF (W.shift "5:Dev")
--   , (className =? "Komodo IDE" <&&> resource =? "Komodo_find2") --> doFloat
--   , (className =? "Komodo IDE" <&&> resource =? "Komodo_gotofile") --> doFloat
--   , (className =? "Komodo IDE" <&&> resource =? "Toplevel") --> doFloat
--   , (className =? "jetbrains-idea") --> doF (W.shift "5:Dev")
--   , (className =? "Empathy") --> doF (W.shift "7:Chat")
--   , (className =? "Skype") --> doF (W.shift "7:Chat")
--   , (className =? "Pidgin") --> doF (W.shift "7:Chat")
--   , (className =? "Gimp-2.8") --> doF (W.shift "9:Amarok")
--   , (className =? "Amarok") --> doF (W.shift "9:Amarok")
--   ]
-- main = do
--   conky1  <- spawnPipe myStatusBar1
--   xmproc1 <- spawnPipe myXmonadBar1
--   conky2  <- spawnPipe myStatusBar2
--   xmproc2 <- spawnPipe myXmonadBar2
--   conky3  <- spawnPipe myStatusBar3
--   xmproc3 <- spawnPipe myXmonadBar3
--   xmonad $ withUrgencyHook NoUrgencyHook $ kde4Config { -- defaultConfig
-- --  xmonad $ withUrgencyHook dzenUrgencyHook { args = ["-bg", "darkgreen", "-xs", "1", "-y", "22"] }
--     focusedBorderColor = myFocusedBorderColor
--   , normalBorderColor = myNormalBorderColor
--   , terminal = myTerminal
--   , borderWidth = myBorderWidth
--   , layoutHook = myLayouts
--   , workspaces = myWorkspaces
--   , modMask = myModMask
--   , handleEventHook = fullscreenEventHook
--   , startupHook = do
--       takeTopFocus >> setWMName "LG3D"
--       windows $ W.greedyView startupWorkspace
--       spawn "~/.xmonad/startup-hook"
--   , manageHook = manageHook kde4Config -- defaultConfig
--       <+> composeAll myManagementHooks
--       <+> manageDocks
--   , logHook = dynamicLogWithPP $ xmobarPP {
--       ppOutput = \s -> do
--           hPutStrLn xmproc1 s
--           hPutStrLn xmproc2 s
--           hPutStrLn xmproc3 s
--       , ppTitle = dzenColor myTitleColor "" . shorten myTitleLength
--       , ppCurrent = dzenColor myCurrentWSColor ""
--         . wrap myCurrentWSLeft myCurrentWSRight
--       , ppVisible = dzenColor myVisibleWSColor ""
--         . wrap myVisibleWSLeft myVisibleWSRight
--       , ppUrgent = dzenColor myUrgentWSColor ""
--         . wrap myUrgentWSLeft myUrgentWSRight
--     }
--   }
--     `additionalKeys` myKeys
