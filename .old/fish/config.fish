set -gx EDITOR nvim
set -gx SYSTEMD_EDITOR /usr/bin/nvim
set -gx BROWSER vivaldi-stable
set -gx TZ 'Europe/Stockholm'

set -gx DOTNET_CLI_TELEMETRY_OPTOUT 1
set -gx MSBuildSDKsPath "/opt/dotnet/sdk/"(dotnet --version)"/Sdks"

set -gx PATH ~/.local/bin $PATH


# Prompt configuration 
set -g fish_prompt_pwd_dir_length 0 # Don't abbreviate path in the prompt
set -g theme_project_dir_length 3 # Abbreviate path within git repos to max 3 chars per level
set -g theme_avoid_ambiguous_glyphs no
set -g theme_powerline_fonts yes
set -g theme_nerd_fonts yes
set -g theme_show_exit_status yes
set -g theme_display_jobs_verbose no
set -g theme_color_scheme terminal-dark
set -g theme_newline_cursor clean
set -g theme_display_date yes
set -g theme_display_cmd_duration no

# Environment status configuration
set -g theme_display_git yes
set -g theme_display_git_dirty yes
set -g theme_display_git_untracked yes
set -g theme_display_git_ahead_verbose yes
set -g theme_display_git_dirty_verbose no
set -g theme_display_git_stashed_verbose no
set -g theme_display_git_master_branch yes
set -g theme_git_worktree_support no
set -g theme_use_abbreviated_branch_name no
set -g theme_display_vagrant no
set -g theme_display_docker_machine nogit s
set -g theme_display_k8s_context no
set -g theme_display_hg no
set -g theme_display_virtualenv yes
set -x VIRTUAL_ENV_DISABLE_PROMPT 1
set -g theme_display_ruby no
set -g theme_display_nvm no
set -g theme_display_vi no

# Terminal window title configuration
set -g theme_title_display_process yes
set -g theme_title_display_path yes
set -g theme_title_use_abbreviated_path yes # e.g. show /home/username as ~
set -g theme_title_display_user no

set -x VIRTUAL_ENV_DISABLE_PROMPT 1

# Glyph overrides - requires nathanchere/bobthefish fork
set -g theme_glyph_backgroundtask ⚙
set -g theme_glyph_errorcode 💥
set -g theme_glyph_superuser ⚡
set -g theme_glyph_git_dirty_staged ✚
set -g theme_glyph_git_dirty_unstaged ●
set -g theme_glyph_git_detached ➦
set -g theme_glyph_git_branch 
set -g theme_glyph_divider 

# We want to load (and unload) .env automatically when detected 
# direnv hook fish | source
function __direnv_export_eval --on-event fish_prompt;
	"/usr/bin/direnv" export fish | source;
end










alias ls='lsd -l'

function about
    echo
    echo -e (uname -ro | awk '{print " \\\\e[1mOS: \\\\e[0;32m"$0"\\\\e[0m"}')
    #echo -e (uptime -p | sed 's/^up //' | awk '{print " \\\\e[1mUptime: \\\\e[0;32m"$0"\\\\e[0m"}')
    echo -e (uname -n | awk '{print " \\\\e[1mHostname: \\\\e[0;32m"$0"\\\\e[0m"}')
    echo -e " \\e[1mDisk usage:\\e[0m"
    # echo
    echo -ne (\
        df -l -h | grep -E 'dev/(nvme|sd|mapper)' | \
        awk '{printf "\\\\t%-10s\\\\t%4s / %4s  %s\\\\n\n", $6, $3, $2, $5}' | \
        sed -e 's/^\(.*\([8][5-9]\|[9][0-9]\)%.*\)$/\\\\e[0;31m\1\\\\e[0m/' -e 's/^\(.*\([7][5-9]\|[8][0-4]\)%.*\)$/\\\\e[0;33m\1\\\\e[0m/' | \
        paste -sd ''\
    )
    # echo

    echo -e " \\e[1mNetwork:\\e[0m"
    # echo
    # http://tdt.rocks/linux_network_interface_naming.html
    echo -ne (\
        ip addr show up scope global | \
            grep -E ': <|inet' | \
            sed \
                -e 's/^[[:digit:]]\+: //' \
                -e 's/: <.*//' \
                -e 's/.*inet[[:digit:]]* //' \
                -e 's/\/.*//'| \
            awk 'BEGIN {i=""} /\.|:/ {print i" "$0"\\\n"; next} // {i = $0}' | \
            sort | \
            column -t -R1 | \
            # public addresses are underlined for visibility \
            sed 's/ \([^ ]\+\)$/ \\\e[4m\1/' | \
            # private addresses are not \
            sed 's/m\(\(10\.\|172\.\(1[6-9]\|2[0-9]\|3[01]\)\|192\.168\.\).*\)/m\\\e[24m\1/' | \
            # unknown interfaces are cyan \
            sed 's/^\( *[^ ]\+\)/\\\e[36m\1/' | \
            # ethernet interfaces are normal \
            sed 's/\(\(en\|em\|eth\)[^ ]* .*\)/\\\e[39m\1/' | \
            # wireless interfaces are purple \
            sed 's/\(wl[^ ]* .*\)/\\\e[35m\1/' | \
            # wwan interfaces are yellow \
            sed 's/\(ww[^ ]* .*\).*/\\\e[33m\1/' | \
            sed 's/$/\\\e[0m/' | \
            sed 's/^/\t/' \
    )
    set_color normal
    echo
    echo "     (╯°□°）╯  ┻━┻      ¯\_(ツ)_/¯"
    echo
end


starship init fish | source
