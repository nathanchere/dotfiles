set -gx EDITOR nvim
set -gx SYSTEMD_EDITOR /usr/bin/nvim
set -gx BROWSER vivaldi-stable
set -gx TZ 'Europe/Stockholm'

set -gx DOTNET_CLI_TELEMETRY_OPTOUT 1
set -gx MSBuildSDKsPath "/usr/share/dotnet/sdk/"(dotnet --version)"/Sdks/"

# Android SDK stuff
set -x ANDROID_HOME $HOME/Android/Sdk
set -x PATH $PATH $ANDROID_HOME/emulator
set -x PATH $PATH $ANDROID_HOME/platform-tools
set -x JAVA_HOME /usr/lib/jvm/java-17-openjdk
set -x PATH $JAVA_HOME/bin $PATH

set -gx PATH ~/.local/bin $PATH
set -gx PATH ~/.dotnet/tools $PATH

# We want to load (and unload) .env automatically when detected
# direnv hook fish | source

set -g fish_color_autosuggestion '555'  'brblack'
set -g fish_color_cancel -r
set -g fish_color_command --bold
set -g fish_color_comment red
set -g fish_color_cwd green
set -g fish_color_cwd_root red
set -g fish_color_end brmagenta
set -g fish_color_error brred
set -g fish_color_escape 'bryellow'  '--bold'
set -g fish_color_history_current --bold
set -g fish_color_host normal
set -g fish_color_match --background=brblue
set -g fish_color_normal normal
set -g fish_color_operator bryellow
set -g fish_color_param cyan
set -g fish_color_quote yellow
set -g fish_color_redirection brblue
set -g fish_color_search_match 'bryellow'  '--background=brblack'
set -g fish_color_selection 'white'  '--bold'  '--background=brblack'
set -g fish_color_user brgreen
set -g fish_color_valid_path --underline

starship init fish | source
source /opt/asdf-vm/asdf.fish

set -x PATH "/home/norfen/bin" $PATH

# >>> conda initialize >>>
# !! Contents within this block are managed by 'conda init' !!
if test -f /home/norfen/.local/share/miniconda3/bin/conda
    eval /home/norfen/.local/share/miniconda3/bin/conda "shell.fish" "hook" $argv | source
else
    if test -f "/home/norfen/.local/share/miniconda3/etc/fish/conf.d/conda.fish"
        . "/home/norfen/.local/share/miniconda3/etc/fish/conf.d/conda.fish"
    else
        set -x PATH "/home/norfen/.local/share/miniconda3/bin" $PATH
    end
end
# <<< conda initialize <<<
