function fish_right_prompt
    if test $CMD_DURATION
        # Show duration of the last command in seconds
        set duration (echo "$CMD_DURATION 1000" | awk '{printf "%.3fs", $1 / $2}')
        echo $duration

        set notify_duration 10000
        set exclude_cmd "bash|less|man|more|ssh"
        if begin
                test $CMD_DURATION -gt $notify_duration
                and echo $history[1] | grep -vqE "^($exclude_cmd).*"
            end
            set name $history[1]
            notify-send "$name finished in $duration"
        end
    end
end
